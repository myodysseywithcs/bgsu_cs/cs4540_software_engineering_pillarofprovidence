# Pillars of Providence

# Developers: Nolan Gormley, Steven Dailey, Kefei Yang, Phuong Nguyen

## Synopsis

Unity Project source files for the 3d real-time stratedy game "Pillars of Providence". Pillars of Providence asks the player to save a city that is on the brink of destruction from natural disaster. The player must deal with multiple trials that test their wit, and challenge them to spend their resources wisely so that they can fight off plagues of destruction. Once the player completes a set amount of trials without letting the Pillar or City be destroyed, the game is won. This project is currently in development and will be regularly updated.

## Motivation

This project is an assignment produced in conjunction with the BGSU digital art students at Bowling Green State University.

## Installation
1. Run `git clone https://gitlab.com/dagormz/CS4540_Pillars.git`
2. Open Unity
3. Select `Open Project` and navigate to the directory `PillarsOfProvidence` and select it
4. If a prompt asks to switch target, accept it and all other prompts until the project appears

## License
> Additional information needed
