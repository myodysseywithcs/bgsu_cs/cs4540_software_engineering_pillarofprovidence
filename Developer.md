# Developer documentation
The intention of this documentation is to explain project requirements, formatting and style.

## General
*  **Authors:** Nolan Gormley, Steven Dailey, Kefei Yang, Phuong Nguyen
*  **Version:** 1.0
*  **Compatibility:** Systems running **Unity** 2017.3.1 or newer

## Requirements
1. **Unity**: 2017.3.1 or newer
2. **Blender**: Not necessary to run project, but used for some asset generation and manipulation

## Installation
1. Run `git clone https://gitlab.com/dagormz/CS4540_Pillars.git`
2. Open Unity
3. Select `Open Project` and navigate to the directory `PillarsOfProvidence` and select it
4. If a prompt asks to switch target, accept it and all other prompts until the project appears

## Code style
Current conventions that are adhered to are as follow;
* 6 spaces for tabs
* K&R brace placement

# Build
## Development environment
1. Open unity and begin editing through Unity
2. Manipulate C# through editor of your choice