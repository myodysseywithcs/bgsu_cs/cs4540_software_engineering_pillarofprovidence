﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class hexMap : MonoBehaviour {

	//Following is a test game object, (TODO)implement all gameobject
	public GameObject cityTile;
	public GameObject grassTile;
	public GameObject darkGrassTile;
	public GameObject clayTile;
	public GameObject dirtTile;
	public GameObject pillar;
	public GameObject botBld1;
	public GameObject botBld2;
	public GameObject botFac;
	public GameObject empBld1;
	public GameObject empBld2;
	public GameObject empFac;
	public GameObject spaBld1;
	public GameObject spaBld2;
	public GameObject spaFac;
	public GameObject botRes;
	public GameObject empRes;
	public GameObject spaRes;


	//give parameter about mapSize which might remove in finalization 
	public int mapSizeColumn;
	public int mapSizeRow;
	public int gameLevel;
	private int ResourceNum;
	public GameObject hexPrefab;
	public GameObject charPrefab;

	/*//materials used
	public Material MatGrass;
	public Material MatDarkGrass;
	public Material MatDirt;
	public Material MatBedrock;
	public Material MatCity;
	public Material MatClay;

	//Mesh used
	public Mesh MeshWater;
	public Mesh MeshFlat;
	public Mesh MeshHill;
	public Mesh MeshMountain;
	*/

	//offset used for character placement
	public Vector3 charOffset;

	//hex store 2D array
	private hexCoordinates[,] hexes;
	private Dictionary<hexCoordinates,GameObject> hexToGameObjectMap;
	private Dictionary<GameObject,hexCoordinates> GameObjectToHexCoor;

	//Following will store area hexes position
	private List<hexCoordinates> tempArea;
	private List<hexCoordinates> cityArea;
	private List<hexCoordinates> grassArea;
	private List<hexCoordinates> darkGrassArea;

	// This will call before start
	void Awake () {
		ResourceNum = GameLevelToResourceNum (gameLevel);
	}

	// Use this for initialization
	void Start () {
		GenerateMap ();
	}

	//This function will return certain hex in the array
	public hexCoordinates GetHexAt(int x, int y)
	{
		if (hexes == null) {
			Debug.LogError ("Hexes array not yet instantiated.");
			return null;
		}
		return hexes [x, y];
	}

	//This function will convert game level to how much resource gonna generate
	private int GameLevelToResourceNum (int gameLevel)
	{
		int ResourceNum;

		if (gameLevel == 1)
			ResourceNum = Random.Range (50, 60);
		else if (gameLevel == 2)
			ResourceNum = Random.Range (40, 50);
		else if (gameLevel == 3)
			ResourceNum = Random.Range (30, 40);
		else
		{
			ResourceNum = 0;
			Debug.Log ("Game level is not valid. Please check script hexMap");
		}

		return ResourceNum;
	}


	//This function will generate the map
	virtual public void GenerateMap()
	{
		//generate a map with dirt
		//Map generation x coordinate is column; y coordinate is row.
		hexes = new hexCoordinates[mapSizeColumn, mapSizeRow];
		hexToGameObjectMap = new Dictionary<hexCoordinates,GameObject> ();
		GameObjectToHexCoor = new Dictionary<GameObject,hexCoordinates> ();

		for (int column = 0; column < mapSizeColumn; column++) {
			for (int row = 0; row < mapSizeRow; row++) {

				hexCoordinates h = new hexCoordinates (column, row);
				hexes [column, row] = h;

				//instantiate gameobjects have four parameters: object name, position (vector3), rotation, transform)
				GameObject hexGO = (GameObject)Instantiate (
					                   hexPrefab,
					                   h.hexPosition (),
					                   Quaternion.identity,
					                   this.transform
				                   );

				//An altered way: hexToGameObjectMap.Add(h,hexGO);
				hexToGameObjectMap [h] = hexGO;
				GameObjectToHexCoor [hexGO] = h;

				hexGO.name = string.Format ("HEX: {0},{1}", column, row);
				hexGO.GetComponentInChildren<TextMesh> ().text = string.Format ("{0},{1}", column, row);

				/*
				MeshRenderer mr = hexGO.GetComponentInChildren<MeshRenderer> ();
				mr.material = MatDirt;

				MeshFilter mf = hexGO.GetComponentInChildren<MeshFilter> ();
				mf.mesh = MeshFlat;
				*/
				//(old version)This line randomize material: mr.material = hexMaterials [Random.Range (0, hexMaterials.Length)]
			}
		}

		//This part replace old tile with object-tile
		int centerCol, centerRow;
		int darkGrassRange, grassRange, cityRange;
		centerCol = mapSizeColumn / 2;
		centerRow = mapSizeRow / 2;
		//following is in case for different mapsize
		//darkGrassRange = (int) 0.9 * centerCol;
		//grassRange = (int) 0.5 * centerCol;
		//cityRange = 5;

		//Dark Grass area
		darkGrassArea = SelectArea (centerCol, centerRow, 26);
		for (int i = 0; i < darkGrassArea.Count; i++)
			ReplaceTile(darkGrassArea[i],darkGrassTile);
		//Grass area
		grassArea = SelectArea (centerCol, centerRow, 15);
		for (int i = 0; i < grassArea.Count; i++)
			ReplaceTile(grassArea[i],grassTile);
		//City area
		cityArea = SelectArea (centerCol, centerRow, 5);
		for (int i = 0; i < cityArea.Count; i++)
			ReplaceTile (cityArea [i], cityTile);
		//Pillar position
		tempArea = SelectArea (centerCol, centerRow, 0);
		for (int i = 0; i < tempArea.Count; i++)
			ReplaceTile (tempArea [i], pillar);

		//Randomized tile texture generate
		int a,b;	//coordinate
		int c;		//Random resource picker
		int NumOfRandTexture;
		hexCoordinates hTemp;
		List<hexCoordinates> LTemp = new List<hexCoordinates>();

		//clay on dark grass
		NumOfRandTexture = 70;
		for (int i = 0; i < NumOfRandTexture; i++) {
			a = Random.Range (0, mapSizeColumn);
			b = Random.Range (0, mapSizeRow);
			hTemp = GetHexAt (a, b);
			//check whether this hTemp is in cityArea or not;
			if (grassArea.Contains (hTemp) || LTemp.Contains(hTemp)||!darkGrassArea.Contains(hTemp)) {
				NumOfRandTexture++;
				continue;
			} else {
				LTemp.Add (hTemp);
				ReplaceTile (hTemp, clayTile);
			}
		}
		//dirt on grass
		NumOfRandTexture =40;
		LTemp.Clear ();
		for (int i = 0; i < NumOfRandTexture; i++) {
			a = Random.Range (0, mapSizeColumn);
			b = Random.Range (0, mapSizeRow);
			hTemp = GetHexAt (a, b);
			//check whether this hTemp is in cityArea or not;
			if (cityArea.Contains (hTemp) || LTemp.Contains(hTemp)||!grassArea.Contains(hTemp)) {
				NumOfRandTexture++;
				continue;
			} else {
				LTemp.Add (hTemp);
				ReplaceTile (hTemp, dirtTile);
			}
		}
			
		//Randomized Resource generate
		LTemp.Clear();
		for (int i = 0; i < ResourceNum; i++) {
			a = Random.Range (0, mapSizeColumn);
			b = Random.Range (0, mapSizeRow);
			hTemp = GetHexAt (a, b);
			//check whether this hTemp is in cityArea or not;
			if (cityArea.Contains (hTemp) || LTemp.Contains(hTemp)||!darkGrassArea.Contains(hTemp)) {
				ResourceNum++;
				continue;
			} else {
				LTemp.Add (hTemp);
				c = Random.Range (1,4);
				if (c == 1)
					ReplaceTile (hTemp, botRes);
				else if (c == 2)
					ReplaceTile (hTemp, empRes);
				else
					ReplaceTile (hTemp, spaRes);
			}
		}
		//following is old stuff
		//GameObject unitGO = (GameObject)Instantiate(charPrefab, hexToGameObjectMap[GetHexAt(1, 1)].transform.position + charOffset + charOffset, Quaternion.identity, hexToGameObjectMap[GetHexAt(1, 1)].transform);
		//following statment can change map to static
		//StaticBatchingUtility.Combine(this.gameObject);
	}

	public List<hexCoordinates> SelectArea(int coorQ, int coorR, int r)
	{
		List<hexCoordinates> SelectedTileList = new List<hexCoordinates>();

		hexCoordinates h;
		int lowLimitQ = coorQ - r;
		int upLimitQ = coorQ + r;
		int upLimitR, numOftilesPerQ;

		if (coorQ %2==0)
		{
			for (int a = lowLimitQ; a <= upLimitQ; a++)
			{
				upLimitR = coorR+r - (int)System.Math.Ceiling(System.Math.Abs(a-coorQ)/2f);
				numOftilesPerQ = 2*r+1 - System.Math.Abs(a - coorQ);

				for (int i = 0, b = upLimitR; i<numOftilesPerQ ;i++, b--)
				{
					h = GetHexAt (a, b);
					SelectedTileList.Add(h);
				}
			}
		}
		else
		{
			for (int a = lowLimitQ; a <= upLimitQ; a++)
			{
				upLimitR = coorR+r - (int)System.Math.Floor(System.Math.Abs(a-coorQ)/2f);
				numOftilesPerQ = 2*r+1 - System.Math.Abs(a - coorQ);

				for (int i = 0, b = upLimitR; i<numOftilesPerQ ;i++, b--)
				{
					h = GetHexAt (a, b);
					SelectedTileList.Add(h);
				}
			}
		}
		return SelectedTileList;
	}

	public void ElevateArea (List<hexCoordinates> SelectArea )
	{
		GameObject hexGO;
		Vector3 offset = new Vector3 (0,0.01f,0);

		for (int i =0, Num = SelectArea.Count; i < Num; i++)
		{
			//Debug.Log (SelectArea [i]);
			hexGO = hexToGameObjectMap[GetHexAt(1,1)];
			hexGO.transform.position += offset;
		}

		//updatePosition = new Vector3 (0, h, 0);
		//hexGO.transform = updatePosition;
	}

	public void ReplaceTile (hexCoordinates hex, GameObject newGO){
		GameObject targetHexTile;

		targetHexTile = hexToGameObjectMap [hex];
		newGO.transform.position = targetHexTile.transform.position;
		newGO.transform.rotation = targetHexTile.transform.rotation;
		targetHexTile.SetActive (false);
		newGO = (GameObject)Instantiate(newGO,newGO.transform.position,newGO.transform.rotation);

		//replace newGO to the old one in dictionary
		hexToGameObjectMap[hex]= newGO;
	}
}
