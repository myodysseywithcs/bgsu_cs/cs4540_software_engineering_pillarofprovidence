﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This class (Hex coordinates) defines the relationship with neighbors, width, heights of itself. This class will not interact with unity.
/// </summary>
public class hexCoordinates {

	public hexCoordinates (int c, int r) {
		this.C = c;
		this.R = r;
		this.T = -(c + r);
	}

	public readonly int C; 	//column
	public readonly int R;	//row
	public readonly int T; 	//Third coordinate

	static readonly float WIDTH_MULTIPILER = Mathf.Sqrt (3) / 2;

	// This function introduce neighbor's location
	public Vector3 hexPosition() {
		
		float radius = 1f;
		float height = WIDTH_MULTIPILER * radius * 2;
		float width = radius * 2;

		float vert = height / 2;
		float horiz = width * 0.75f;
		float x, y, z;

		//The column-modified version of vector3: the odd column will all set off from 1/2 vertical from the origin tile. 
		if (this.C % 2f == 1f) {
			x = horiz * this.C; 
			y = 0;
			z = vert * (this.R * 2f + 1f);
		} 
		//the even column will all set off from the origin tile
		else {
			x = horiz * this.C; 
			y = 0;
			z = vert * (this.R * 2f);
		}


		/* This is the column-unmodified version of vector3
		 * x = horiz * this.C; 
		 * y = 0;
		 * z = vert * (this.R * 2f+ this.C);
		 */
		 
		return new Vector3(x,y,z);
	}
}
