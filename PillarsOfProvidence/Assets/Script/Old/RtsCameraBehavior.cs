﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class RtsCameraBehavior : MonoBehaviour {

	public float panSpeed = 3f;
	public float rotSpeed = 10f;
	public float panBorder = 20f;
	private float dist;
	private float rotation = 0f;
	private Vector3 MouseStart, MouseMove;
	Vector3 cameraTargetOffset;

	// Use this for initialization
	void Start () {

	}

	Vector3 MouseToGroundPlane(Vector3 mousePos) {
		Ray mouseRay = Camera.main.ScreenPointToRay (mousePos);

		// What is the point at which the mouse ray intersects Y=0
		if (mouseRay.direction.y >= 0) {
			return Vector3.zero;
		}
		float rayLength = (mouseRay.origin.y / mouseRay.direction.y);
		return mouseRay.origin - (mouseRay.direction * rayLength);
	}

	void Update_ScrollZoom() {
		// Zoom to scrollwheel
		float scrollAmount = Input.GetAxis ("Mouse ScrollWheel");
		float minHeight = 2;
		float maxHeight = 20;

		// Move camera towards hitPos
		Vector3 hitPos = MouseToGroundPlane(Input.mousePosition);
		Vector3 dir = hitPos - Camera.main.transform.position;
		Vector3 p = Camera.main.transform.position;

		// Stop zooming out at a certain distance.
		if (scrollAmount > 0 || p.y < (maxHeight - 0.1f)) {
			cameraTargetOffset += dir * scrollAmount;
		}

		Vector3 lastCameraPosition = Camera.main.transform.position;
		Camera.main.transform.position = Vector3.Lerp(Camera.main.transform.position,
			Camera.main.transform.position + cameraTargetOffset, Time.deltaTime * 5f
		);
		cameraTargetOffset -= Camera.main.transform.position - lastCameraPosition;

		p = Camera.main.transform.position;
		if (p.y < minHeight) {
			p.y = minHeight;
		}
		if (p.y > maxHeight) {
			p.y = maxHeight;
		}
		Camera.main.transform.position = p;

		// Change camera angle
		Camera.main.transform.rotation = Quaternion.Euler (
			Mathf.Lerp (30, 75, Camera.main.transform.position.y / maxHeight),
			Camera.main.transform.rotation.eulerAngles.y,
			Camera.main.transform.rotation.eulerAngles.z
		);

	}

	void UpdateMovement () {
		Vector3 position = transform.position;

		if (Input.GetKey ("w") || Input.mousePosition.y > Screen.height - panBorder) {
			position.z += panSpeed * Time.deltaTime;
		}
		if (Input.GetKey ("s") || Input.mousePosition.y < panBorder) {
			position.z -= panSpeed * Time.deltaTime;
		}
		if (Input.GetKey ("d") || Input.mousePosition.x > Screen.width - panBorder) {
			position.x += panSpeed * Time.deltaTime;
		}
		if (Input.GetKey ("a") || Input.mousePosition.x < panBorder) {
			position.x -= panSpeed * Time.deltaTime;
		}

		dist = Camera.main.transform.position.y;

		if (Input.GetMouseButtonDown (0)) {
			MouseStart = new Vector3(Input.mousePosition.x, dist, Input.mousePosition.y);
		}
		else if (Input.GetMouseButton (0)) {
			MouseMove = new Vector3(Input.mousePosition.x - MouseStart.x, dist,
				Input.mousePosition.y - MouseStart.z
			);
			MouseStart = new Vector3(Input.mousePosition.x, dist, Input.mousePosition.y);
			position = new Vector3(transform.position.x - MouseMove.x * Time.deltaTime,
				dist, transform.position.z - MouseMove.z * Time.deltaTime
			);
		}

		transform.position = position;
	}

	void UpdateRotation () {
		if (Input.GetKey ("q")) {
			rotation -= rotSpeed * Time.deltaTime;
		}
		if (Input.GetKey ("e")) {
			rotation += rotSpeed * Time.deltaTime;
		}
		transform.localEulerAngles = new Vector3 (Camera.main.transform.rotation.eulerAngles.x, -rotation,
			Camera.main.transform.rotation.eulerAngles.z
		);
	}

	void Update () {
		Update_ScrollZoom ();
		UpdateMovement ();
		UpdateRotation ();
	}
}
