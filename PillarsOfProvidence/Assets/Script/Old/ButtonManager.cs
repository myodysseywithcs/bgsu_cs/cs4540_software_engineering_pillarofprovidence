﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonManager : MonoBehaviour {

	public static GameObject buttonInstance;

	void Awake(){
		if (buttonInstance != null){
			Debug.LogError("More than one ButtonManager in scene!");
			return;
		}
		buttonInstance = this.gameObject;
	}

	public void Clicked() {
		if (buttonInstance.gameObject.active)
			buttonInstance.gameObject.SetActive(false);
		else
			buttonInstance.gameObject.SetActive(true);
	}
}
