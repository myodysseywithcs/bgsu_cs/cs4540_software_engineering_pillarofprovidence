﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlacementTileScript : MonoBehaviour {

	public Vector3 positionOffset;
	private GameObject BldToPlace;

	BuildManager buildManager;
	void Start () {
		buildManager = BuildManager.instance;
	}
	


	void OnMouseDown () {
		if(buildManager.GetTurretToBuild() == null)
		return;

		//hexCoordinates h;
		//h = hexMap.GameObjectToHexCoor[BldToPlace];

		if(BldToPlace != null)//|| !hexMap.cityArea.Contains(h))
		{ 
			Debug.Log("Can't build there! ");
			return;
		}

		GameObject turretToBuild = buildManager.GetTurretToBuild();
		BldToPlace = (GameObject)Instantiate(turretToBuild,transform.position+positionOffset,transform.rotation);
		buildManager.SetTurretToBuild(null);
	}

}
