﻿using UnityEngine;

public class Shop : MonoBehaviour {

	BuildManager buildManager;
	// Use this for initialization
	void Start () {
		buildManager = BuildManager.instance;
	}
	
	public void BuildBot1(){
		Debug.Log("Building BotBld1 Purchased");
		buildManager.SetTurretToBuild(buildManager.BotBld1);
	}

	public void BuildBot2(){
		Debug.Log("Building BotBld2 2 Purchased");
		buildManager.SetTurretToBuild(buildManager.BotBld2);
	}

	public void BuildBotFac(){
		Debug.Log("Building BotFac Purchased");
		buildManager.SetTurretToBuild(buildManager.BotFac);
	}

		public void BuildEmp1(){
		Debug.Log("Building EmpBld1 Purchased");
		buildManager.SetTurretToBuild(buildManager.EmpBld1);
	}

	public void BuildEmp2(){
		Debug.Log("Building EmpBld2 Purchased");
		buildManager.SetTurretToBuild(buildManager.EmpBld2);
	}

	public void BuildEmpFac(){
		Debug.Log("Building EmpFac Purchased");
		buildManager.SetTurretToBuild(buildManager.EmpFac);
	}

		public void BuildSpa1(){
		Debug.Log("Building SpaBld1 1 Purchased");
		buildManager.SetTurretToBuild(buildManager.SpaBld1);
	}

	public void BuildSpa2(){
		Debug.Log("Building SpaBld2 2 Purchased");
		buildManager.SetTurretToBuild(buildManager.SpaBld2);
	}

	public void BuildSpaFac(){
		Debug.Log("Building SpaFac Purchased");
		buildManager.SetTurretToBuild(buildManager.SpaFac);
	}
}
