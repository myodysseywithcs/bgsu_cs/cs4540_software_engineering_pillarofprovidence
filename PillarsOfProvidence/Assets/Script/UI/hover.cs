﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class hover : MonoBehaviour {

    setCursor cursor;
	private Color startColor;

	// Use this for initialization
	void Start () {
        cursor = GameObject.FindGameObjectWithTag("MainScript").GetComponent<setCursor>();
    }

    // Update is called once per frame
    void Update ()
    {

    }

    void OnMouseEnter()
    {
		//texture change
		if (gameObject.tag == "touch")
			cursor.setTouchCurTexture ();
		else if (gameObject.tag == "ore")
			cursor.setOreCurTexture ();
		else if (gameObject.tag == "stone")
			cursor.setStoneCurTexture ();
		else if (gameObject.tag == "circuit")
            cursor.setCirCurTexture();

		//Object pop up
		transform.Translate (Vector3.up, Space.Self);

		//object hightlight
		GetComponent<Renderer> ().material.shader = Shader.Find ("Self-Illumin/Outlined Diffuse");
    }

    void OnMouseExit()
    {
		//texture change
        cursor.setDefaut();

		//Object pop up
		transform.Translate (Vector3.down, Space.Self);

		//object hightlight
		GetComponent<Renderer> ().material.shader = Shader.Find ("Diffuse");

    }
}
