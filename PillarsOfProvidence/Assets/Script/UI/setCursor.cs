﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class setCursor : MonoBehaviour {

    public Texture2D defaultCur;
    public Texture2D touchCur;
    public Texture2D oreCur;
    public Texture2D cirCur;
    public Texture2D stoneCur;
    public CursorMode curMode = CursorMode.Auto;
    public Vector2 hotSpot = Vector2.zero;

    // Use this for initialization
    void Start () {
        Cursor.SetCursor(defaultCur, hotSpot, curMode);
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void setDefaut()
    {
        Cursor.SetCursor(defaultCur, hotSpot, curMode);
    }
	public void setTouchCurTexture()
    {
        Cursor.SetCursor(touchCur, hotSpot, curMode);
    }
    public void setOreCurTexture()
    {
        Cursor.SetCursor(oreCur, hotSpot, curMode);
    }
    public void setStoneCurTexture()
    {
        Cursor.SetCursor(stoneCur, hotSpot, curMode);
    }
	public void setCirCurTexture()
    {
		Cursor.SetCursor(cirCur, hotSpot, curMode);
    }
}
