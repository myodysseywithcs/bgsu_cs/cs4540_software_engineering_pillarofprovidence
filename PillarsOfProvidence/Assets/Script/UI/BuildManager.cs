﻿using UnityEngine;

public class BuildManager : MonoBehaviour {

	public static BuildManager instance;

	void Awake(){
		if (instance != null){
			Debug.LogError("More than one BuildManager in scene!");
			return;
		}
		instance = this;
	}

	public GameObject BotBld1;
	public GameObject BotBld2;
	public GameObject BotFac;
	public GameObject EmpBld1;
	public GameObject EmpBld2;
	public GameObject EmpFac;	
	public GameObject SpaBld1;
	public GameObject SpaBld2;
	public GameObject SpaFac;
	private GameObject turretToBuild;

	public GameObject GetTurretToBuild(){
		return turretToBuild;
	}

	public void SetTurretToBuild (GameObject building){
		turretToBuild = building;
	}
}
