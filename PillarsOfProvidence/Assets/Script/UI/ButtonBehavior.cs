using UnityEngine;
using System.Collections;

public class ButtonBehavior : MonoBehaviour {

	public GameObject PopUpMenu;
	private bool isActive;

	/*public void Clicked() {
		if (PopUpMenu.active)
			PopUpMenu.SetActive(false);
		else
			PopUpMenu.SetActive(true);
	}*/


	public void Clicked() {
		isActive = PopUpMenu.activeSelf;
		isActive = !isActive;
		PopUpMenu.SetActive (isActive);
	}
}