﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor.SceneManagement;
#endif

public class PauseMenu : MonoBehaviour {

	public static bool gameIsPause = false;
	public GameObject PauseMenuUI;

	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.Escape)) {
			if (gameIsPause) {
				Resume ();
			}
			else {
				Pause ();
			}
		}
	}

	public void Resume() {
		PauseMenuUI.SetActive (false);
		Time.timeScale = 1f;
		gameIsPause = false;
	}

	void Pause() {
		PauseMenuUI.SetActive (true);
		Time.timeScale = 0f;
		gameIsPause = true;
	}

	public void SaveAndQuit(){
		Time.timeScale = 1f;
		EditorSceneManager.LoadScene (0);
		gameIsPause = false;
	}
}
